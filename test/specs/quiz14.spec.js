
describe.only('My Sign up application', () => {
    it.only('sign up with valid credentials', async () => {
        //Sign up 
        await driver.$("~Login").click();
        await new Promise(resolve => setTimeout(resolve, 5000));
        const actualText1 = await $('.android.widget.TextView')
        await expect(actualText1).toHaveText('Login / Sign up Form')
        await driver.$("~button-sign-up-container").click();
        await driver.$("~input-email").click();
        await driver.$("~input-email").setValue("ari110188@gmail.com");
        await driver.$("~input-password").click();
        await driver.$("~input-password").setValue("Januari88");
        await driver.$("~input-repeat-password").click();
        await driver.$("~input-repeat-password").setValue("Januari88");
        await driver.$("~button-SIGN UP").click();
        await new Promise(resolve => setTimeout(resolve, 5000));
        const actualText2 = await $('android.widget.TextView')
        await expect(actualText2).toHaveText('Signed Up!')
        await new Promise(resolve => setTimeout(resolve, 5000));
        await driver.$("android.widget.Button").click();
        

    })

    it.only('sign up with invalid credentials', async () => {
        //Sign up
        await driver.$("~input-email").click();
        await driver.$("~input-email").clearValue();
        await driver.$("~input-email").setValue("ari110188@");
        await driver.$("~input-password").click();
        await driver.$("~input-password").clearValue();
        await driver.$("~input-password").setValue("Januari");
        await driver.$("~input-repeat-password").click();
        await driver.$("~input-repeat-password").clearValue();
        await driver.$("~input-repeat-password").setValue("Januari");
        await driver.$("~button-SIGN UP").click();
        const actualText5 = await $('//android.widget.ScrollView[@content-desc="Login-screen"]/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup[4]/android.widget.TextView[1]')
        await expect(actualText5).toHaveText('Please enter a valid email address')
        const actualText6 = await $('//android.widget.ScrollView[@content-desc="Login-screen"]/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup[4]/android.widget.TextView[2]')
        await expect(actualText6).toHaveText('Please enter at least 8 characters')
        const actualText7 = await $('//android.widget.ScrollView[@content-desc="Login-screen"]/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup[4]/android.widget.TextView[3]')
        await expect(actualText7).toHaveText('Please enter the same password')

    

    })

})


